bl_info = {
    "name": "Multikey",
    "author": "Tal Hershkovich ",
    "version": (0, 4),
    "blender": (3, 2, 0),
    "location": "View3D - Properties - Animation Panel",
    "description": "Edit Multiply Keyframes by adjusting their value or randomizing it",
    "warning": "",
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Animation/Multikey",
    "category": "Animation"}

if "bpy" in locals():
    import importlib
    if "multikey" in locals():
        importlib.reload()
        
import bpy
import random
import numpy as np
from mathutils import Quaternion

def attr_default(obj, fcu_key):
    #check if the fcurve source belongs to a bone or obj
    if  fcu_key[0][:10] == 'pose.bones':
        transform = fcu_key[0].split('.')[-1]
        attr = fcu_key[0].split('"')[-2]
        bone = fcu_key[0].split('"')[1]
        source = obj.pose.bones[bone]
        
    #in case of shapekey animation        
    elif fcu_key[0][:10] == 'key_blocks':
        attr = fcu_key[0].split('"')[1]
        shapekey = obj.data.shape_keys.key_blocks[attr]
        return 0 if shapekey.slider_min <= 0 else shapekey.slider_min
    #in case of transforms in object mode
    else:# fcu_key[0] in transform_types:
        source = obj
        transform = fcu_key[0]
        
    #check when it's transform property of Blender
    if transform in source.bl_rna.properties.keys():
        if hasattr(source.bl_rna.properties[transform], 'default_array'):
            if len(source.bl_rna.properties[transform].default_array) > fcu_key[1]:
                attrvalue = source.bl_rna.properties[transform].default_array[fcu_key[1]]
                return attrvalue

    #in case of property on object
    elif fcu_key[0].split('"')[1] in obj.keys():
        attr = fcu_key[0].split('"')[1]

    if 'attr' not in locals():
        print(fcu_key[0], 'has no attributes returning 0')
        return 0  
      
    #since blender 3 access to custom property settings changed
    if attr in source:
        id_attr = source.id_properties_ui(attr).as_dict()
        attrvalue = id_attr['default']
        return attrvalue
    
    return 0

def store_handles(key):
    #storing the distance between the handles bezier to the key value
    handle_r = key.handle_right[1] - key.co[1]
    handle_l = key.handle_left[1] - key.co[1]
    
    return handle_r, handle_l

def apply_handles(key, handle_r, handle_l):
    key.handle_right[1] = key.co[1] + handle_r
    key.handle_left[1] = key.co[1] + handle_l

def selected_bones_filter(obj, fcu_data_path):
    if not bpy.context.scene.multikey.selectedbones:
    #if not obj.als.onlyselected:
        return False
    if obj.mode != 'POSE':
        return True
    transform_types = ['location', 'rotation_euler', 'rotation_quaternion', 'scale']
    #filter selected bones if option is turned on
    bones = [bone.path_from_id() for bone in bpy.context.selected_pose_bones]
    if fcu_data_path.split('].')[0]+']' not in bones and fcu_data_path not in transform_types:
        return True

def filter_properties(obj, fcu):
    'Filter the W X Y Z attributes of the transform properties'
    transform = fcu.data_path.split('"].')[1] if obj.mode == 'POSE' else fcu.data_path
    index = fcu.array_index
    if 'rotation' in transform:
        if transform == 'rotation_euler':
            index -= 1
        transform = 'rotation'
    transform = 'filter_' + transform
    attr = getattr(bpy.context.scene.multikey, transform)
    #print(fcu.data_path, index, transform, attr[index])
    return True if attr[index] else False
        
def add_value(key, value):
    if key.select_control_point:
        #store handle values in relative to the keyframe value
        handle_r, handle_l = store_handles(key)              
        
        key.co[1] += value
        apply_handles(key, handle_r, handle_l)
                
#calculate the difference between current value and the fcurve value                
def add_diff(obj, fcurves, path, current_value, eval_array):    
    array_value = current_value - eval_array
    if not any(array_value):
        return
    for i, value in enumerate(array_value):
        fcu = fcurves.find(path, index = i)
        if fcu is None or not filter_properties(obj, fcu):
            continue
        for key in fcu.keyframe_points:
            add_value(key, value)
        fcu.update()
        
def scale_value(self, context):
    
    for obj in context.selected_objects:
        if obj.animation_data.action is None:
            continue
        action = obj.animation_data.action
        for fcu in action.fcurves:
            if obj.mode == 'POSE':
                if selected_bones_filter(obj, fcu.data_path):
                    continue
                # for bone in context.selected_pose_bones:
                #     #find the fcurve of the bone
                #     if fcu.data_path.rfind(bone.name) == 12 and fcu.data_path[12 + len(bone.name)] == '"':
                #         function(fcu)                           
            if not filter_properties(obj, fcu):
                continue
            value_list = []
            scale = bpy.context.scene.multikey.scale
            for key in fcu.keyframe_points: 
                if key.select_control_point == True: 
                    value_list.append(key.co[1])
            if len(value_list)>1:
                #the average value with the scale property added to it
                avg_value = sum(value_list) / len(value_list)
                for key in fcu.keyframe_points:
                    if key.select_control_point == True:
                        #store handle_type 
                        handle_r, handle_l = store_handles(key)
                        #add the value of the distance from the average * scale factor
                        key.co[1] = avg_value + ((key.co[1] - avg_value)*scale)
                        key = apply_handles(key, handle_r, handle_l)
                fcu.update()
    
def random_value(self, context):
    
    for obj in context.selected_objects:
        if obj.animation_data.action is None:
            continue
        action = obj.animation_data.action
        for fcu in action.fcurves:
            if obj.mode == 'POSE':
                if selected_bones_filter(obj, fcu.data_path):
                    continue
            if not filter_properties(obj, fcu):
                continue
            value_list = []
            threshold = bpy.context.scene.multikey.randomness                        
            for key in fcu.keyframe_points: 
                if key.select_control_point == True: 
                    value_list.append(key.co[1])
                    
            if len(value_list) > 0:
                value = max(value_list)- min(value_list)
                for key in fcu.keyframe_points:
                    add_value(key, value * random.uniform(-threshold, threshold))
                fcu.update()
                                
def evaluate_combine(data_path, added_array, eval_array, array_default, influence):
    
    if 'scale' in data_path:
        eval_array = eval_array * (added_array / array_default) ** influence
    elif 'rotation_quaternion' in data_path:
        #multiply first the influence with the w separatly
        added_array[0] = added_array[0] + (1- added_array[0])*(1 - influence)
        added_array[1:] *= influence 
        eval_array = np.array(Quaternion(eval_array) @ Quaternion(added_array))# ** influence
    #if it's a custom property
    elif 'rotation_euler' not in data_path and 'location' not in data_path:
        eval_array = eval_array + (added_array - array_default) * influence
        
    return eval_array       

def evaluate_array(action, fcu_path, frame, array_len):
    '''Create an array from all the indexes'''
    
    fcu_array = []
    for i in range(array_len):
        fcu = action.fcurves.find(fcu_path, index = i)
        if fcu is None:
            continue
        fcu_array.append(fcu.evaluate(frame))
    if not len(fcu_array):
        return None
    return np.array(fcu_array)
    
def evaluate_layers(context, obj, anim_data, fcu_path, array_len):
    '''Calculate the evaluation of all the layers when using the nla'''
    
    if not hasattr(anim_data, 'nla_tracks') or not anim_data.use_nla:
        return None
    nla_tracks = anim_data.nla_tracks
    if not len(nla_tracks):
        return None
    frame = context.scene.frame_current
    blend_types = {'ADD' : '+', 'SUBTRACT' : '-', 'MULTIPLY' : '*'}
    
    #eval_array = np.zeros(array_len) if 'scale' not in fcu_path else np.ones(array_len)
    array_default = np.array([attr_default(obj, (fcu_path, i)) for i in range(4) if anim_data.action.fcurves.find(fcu_path, index = i) is not None])
    eval_array = array_default
    
    for track in nla_tracks:
        if track.mute:
            continue
        for strip in track.strips: 
            if not strip.frame_start < frame < strip.frame_end:
                continue
            action = strip.action
            blend_type = strip.blend_type 
            
            #get the influence value either from the attribute or the fcurve. function coming from bake
            if not strip.fcurves[0].mute and len(strip.fcurves[0].keyframe_points):
                influence = strip.fcurves[0].evaluate(frame)
            else:
                influence = strip.influence
            
            #evaluate the frame according to the strip settings
            frame_eval = frame
            #change the frame if the strip is on hold    
            if frame < strip.frame_start:
                if strip.extrapolation == 'HOLD':
                    frame_eval = strip.frame_start
            elif frame >= strip.frame_end:
                if strip.extrapolation == 'HOLD' or strip.extrapolation == 'HOLD_FORWARD':
                    frame_eval = strip.frame_end
                    
            last_frame = strip.frame_start + (strip.frame_end - strip.frame_start) / strip.repeat
            
            if strip.repeat > 1 and (frame) >= last_frame:
                action_range = (strip.action_frame_end * strip.scale - strip.action_frame_start * strip.scale)
                frame_eval = (((frame_eval - strip.frame_start) % (action_range)) + strip.frame_start)
                        
            if strip.use_reverse:
                frame_eval = last_frame - (frame_eval - strip.frame_start)
            offset = (strip.frame_start * 1/strip.scale - strip.action_frame_start) * strip.scale
            frame_eval = strip.frame_start * 1/strip.scale + (frame_eval - strip.frame_start) * 1/strip.scale - offset * 1/strip.scale
            
            fcu_array = evaluate_array(action, fcu_path, frame, array_len)
            if fcu_array is None:
                continue
             ###EVALUATION###
            if blend_type =='COMBINE':
                if 'location' in fcu_path or 'rotation_euler' in fcu_path:
                    blend_type = 'ADD'
            if blend_type =='REPLACE':
                eval_array = eval_array * (1 - influence) + fcu_array * influence
            elif blend_type =='COMBINE':
                eval_array = evaluate_combine(fcu_path, fcu_array, eval_array, array_default, influence)
            else:
                eval_array = eval('eval_array' + blend_types[blend_type] + 'fcu_array' + '*' + str(influence))
    
    return eval_array              

def evaluate_value(self, context):
    for obj in context.selected_objects:
        
        anim_data = obj.animation_data
        if anim_data is None:
            return
        if anim_data.action is None:
            return
        #print(obj.animation_data.action)
        
        action = obj.animation_data.action
        fcu_paths = []
        transformations = ["rotation_quaternion","rotation_euler", "location", "scale"]
        if obj.mode == 'POSE': 
            bonelist = context.selected_pose_bones if obj.als.onlyselected else obj.pose.bones
            
        for fcu in action.fcurves:
            if fcu in fcu_paths:
                continue
            if obj.mode == 'POSE':   
                if selected_bones_filter(obj, fcu.data_path):
                    continue
                
                for bone in bonelist:
                    #find the fcurve of the bone
                    if fcu.data_path.rfind(bone.name) != 12 or fcu.data_path[12 + len(bone.name)] != '"': 
                        continue
                    transform = fcu.data_path[15 + len(bone.name):]
                    if transform not in transformations:
                        continue
                    current_value = getattr(obj.pose.bones[bone.name], transform)
            else:
                transform = fcu.data_path
                current_value = getattr(obj, transform)
                
            eval_array = evaluate_layers(context, obj, anim_data, fcu.data_path, len(current_value))
            if eval_array is None:
                eval_array = evaluate_array(action, fcu.data_path, context.scene.frame_current, len(current_value))
            #calculate the difference between current value and the fcurve value 
            add_diff(obj, action.fcurves, fcu.data_path, np.array(current_value), eval_array)
                            
class MULTIKEY_OT_Multikey(bpy.types.Operator):
    """Edit all selected keyframes"""
    bl_label = "Edit Selected Keyframes"
    bl_idname = "fcurves.multikey"
    bl_options = {'REGISTER', 'UNDO'}  
    
    
    #bpy.types.Scene.selectedbones = bpy.props.BoolProperty(name="Affect only selected bones", description="Affect only selected bones", default=True, options={'HIDDEN'})
    
    #bpy.types.Scene.handletype = bpy.props.BoolProperty(name="Keep handle types", description="Keep handle types", default=False, options={'HIDDEN'})
    
    @classmethod
    def poll(cls, context):
        return context.active_object and context.active_object.animation_data and bpy.context.scene.tool_settings.use_keyframe_insert_auto == False
      
    def execute(self, context):
        evaluate_value(self, context)
        return {'FINISHED'} 
 
class MultikeyProperties(bpy.types.PropertyGroup):
    
    selectedbones: bpy.props.BoolProperty(name="Affect only selected bones", description="Affect only selected bones", default=True, options={'HIDDEN'})
    handletype: bpy.props.BoolProperty(name="Keep handle types", description="Keep handle types", default=False, options={'HIDDEN'})
    scale: bpy.props.FloatProperty(name="Scale Factor", description="Scale percentage of the average value", default=1.0, update = scale_value)
    randomness: bpy.props.FloatProperty(name="Randomness", description="Random Threshold of keyframes", default=0.1, min=0.0, max = 1.0, update = random_value)
    #filters
    filter_location: bpy.props.BoolVectorProperty(name="Location", description="Filter Location properties", default=(False, True, True), size = 3, options={'HIDDEN'})
    filter_rotation: bpy.props.BoolVectorProperty(name="Rotation", description="Filter Rotation properties", default=(True, True, True, True), size = 4, options={'HIDDEN'})
    filter_scale: bpy.props.BoolVectorProperty(name="Scale", description="Filter Scale properties", default=(True, True, True), size = 3, options={'HIDDEN'})

class FilterProperties(bpy.types.Operator):
    """Filter Location Rotation and Scale Properties"""
    bl_idname = "fcurves.filter"
    bl_label = "Filter Properties               W    X    Y    Z"
    bl_options = {'REGISTER', 'UNDO'}
    
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width = 200)
    
    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text = 'Location')
        row.prop(context.scene.multikey, 'filter_location', text = '')
        row = layout.row()
        row.label(text = 'Rotation')
        row.prop(context.scene.multikey, 'filter_rotation', text = '')
        row = layout.row()
        row.label(text = 'Scale')
        row.prop(context.scene.multikey, 'filter_scale', text = '')
    
    def execute(self, context):
        return {'CANCELLED'}
        
class MultikeyPanel(bpy.types.Panel):
    """Add random value to selected keyframes"""
    bl_label = "Multikey"
    bl_idname = "SCENE_PT_layout"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category= 'Animation'
    
    def draw(self, context): 
        layout = self.layout
        layout.prop(context.scene.multikey, 'selectedbones')
        #layout.prop(context.scene.multikey, 'handletype') 
        layout.separator()
        #layout.label(text="Edit all selected keyframes")
        split = layout.split(factor=0.1, align = True)
        split.operator('fcurves.filter', icon ='FILTER', text = '')
        split.operator("fcurves.multikey", icon = 'ACTION_TWEAK')
        layout.separator()
        #layout.label(text="Scale Keyframe Values")
        #layout.operator("fcurves.scalevalues", icon = 'CON_SIZELIMIT')
        layout.prop(context.scene.multikey, 'scale')
        #layout.separator()
        #layout.label(text="Randomize selected keyframes")
        #layout.operator("fcurves.randomizekeys")
        layout.prop(context.scene.multikey, 'randomness', slider = True)       

classes = (MultikeyPanel, MultikeyProperties, FilterProperties, MULTIKEY_OT_Multikey) # RANDOMIZE_OT_RandomizeKeys, SCALE_OT_ScaleValues

#register, unregister = bpy.utils.register_classes_factory(classes)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
    bpy.types.Scene.multikey = bpy.props.PointerProperty(type = MultikeyProperties, options={'LIBRARY_EDITABLE'}, override = {'LIBRARY_OVERRIDABLE'})

def unregister():
    from bpy.utils import unregister_class
    for cls in classes:
        unregister_class(cls)
    del bpy.types.Scene.multikey

if __name__ == "__main__":
    register()                               